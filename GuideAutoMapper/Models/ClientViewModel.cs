﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace GuideAutoMapper.Models
{
    public class ClientViewModel
    {
        public string Name { get; set; }

        public string LastName { get; set; }

        public string Email { get; set; }
    }
}