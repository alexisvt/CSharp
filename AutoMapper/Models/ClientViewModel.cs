﻿
namespace AutoMapper.Models
{
    public class ClientViewModel
    {
        public string Name { get; set; }

        public string LastName { get; set; }

        public string Email { get; set; }
    }
}