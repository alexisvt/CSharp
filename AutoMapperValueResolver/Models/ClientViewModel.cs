﻿
namespace AutoMapperValueResolver.Models
{
    public class ClientViewModel
    {
        public string Name { get; set; }

        public string Email { get; set; }
    }
}